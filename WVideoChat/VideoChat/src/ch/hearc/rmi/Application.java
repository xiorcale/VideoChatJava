
package ch.hearc.rmi;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import ch.hearc.encryption.Message;
import ch.hearc.gui.chat.text.JPanelReceive;
import ch.hearc.gui.chat.text.JPanelSend;
import ch.hearc.gui.chat.video.videopanel.JPanelRemoteVideo;
import ch.hearc.specifications.rmi.Application_I;
import ch.hearc.video.image.WebcamImage;

import com.bilat.tools.reseau.rmi.RmiTools;
import com.bilat.tools.reseau.rmi.RmiURL;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class Application implements Application_I ,Runnable
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public static synchronized Application getInstance()
		{
		if (INSTANCE == null)
			{
			INSTANCE = new Application();
			}
		return INSTANCE;
		}

	public static void init(String serverName, String pseudo)
		{
		SERVER_NAME = serverName;
		PSEUDO = pseudo;
		}

	private Application()
		{
		//No RemoteInstance when launched
		this.remoteInstance = null;

		//Create private/public key
		generateKeysSecurity();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	public void sendMessage(String message)
		{
		try
			{
			String messageFormated = PSEUDO + ": " + message;

			jPanelReceive.addText(messageFormated);
			remoteInstance.readMessage(new Message(messageFormated));

			jPanelSend.clearText();
			}
		catch (RemoteException e)
			{
			e.printStackTrace();
			}
		}

	public void sendImage(WebcamImage webcamImage)
		{
		try
			{
			remoteInstance.readImage(webcamImage);
			}
		catch (RemoteException e)
			{
			e.printStackTrace();
			}
		}

	@Override
	public void run()
		{
		System.out.println("[Application]:run");
		serverSide();
		clientSide();
		}

	@Override
	public void readMessage(Message message)
		{
		jPanelReceive.addText(message.getMessage());
		}

	@Override
	public void readImage(WebcamImage webcamImage)
		{
		jPanelRemoteVideo.setRemoteWebcamImage(webcamImage);
		}

	@Override
	public void savePublicKey(PublicKey _remotePublicKey)
		{
		remotePublicKey = _remotePublicKey;
		}

	/*------------------------------*\
	|*				Set				*|
	\*------------------------------*/

	public void setjPanelReceive(JPanelReceive jPanelReceive)
		{
		this.jPanelReceive = jPanelReceive;
		}

	public void setjPanelSend(JPanelSend jPanelSend)
		{
		this.jPanelSend = jPanelSend;
		}

	public void setJPanelRemoteVideo(JPanelRemoteVideo jPanelRemoteVideo)
		{
		this.jPanelRemoteVideo = jPanelRemoteVideo;
		}

	/*------------------------------*\
	|*				Get				*|
	\*------------------------------*/

	public PublicKey getRemotePublicKey()
		{
		return remotePublicKey;
		}

	public PrivateKey getPrivateKey()
		{
		return privateKey;
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void generateKeysSecurity()
		{
		KeyPairGenerator keyGen = null;
		try
			{
			keyGen = KeyPairGenerator.getInstance("RSA");
			}
		catch (NoSuchAlgorithmException e)
			{
			e.printStackTrace();
			}
		keyGen.initialize(1024);

		KeyPair pair = keyGen.generateKeyPair();

		privateKey = pair.getPrivate();
		publicKey = pair.getPublic();

		}

	private void serverSide()
		{
		try
			{
			RmiURL rmiURL = new RmiURL(SettingsRMI.APPLICATION_ID, SettingsRMI.APPLICATION_PORT);
			RmiTools.shareObject(this, rmiURL);
			RmiTools.afficherAllShareObject(1099);
			}
		catch (RemoteException e)
			{
			System.err.println("[Application]:serverSide:share failed");
			e.printStackTrace();
			}
		}

	private void clientSide()
		{
		Application_I application = connect();
		IS_CONNECTED_TO_REMOTE_INSTANCE = true;
		remoteInstance = application;

		//Send the privateKey to remoteInstance
		try
			{
			remoteInstance.savePublicKey(this.publicKey);
			}
		catch (RemoteException e)
			{
			e.printStackTrace();
			}
		}

	private Application_I connect()
		{
		try
			{
			long delayMs = 1000L;
			int nbTentativeMax = 100;

			RmiURL rmiURL = new RmiURL(SettingsRMI.APPLICATION_ID, InetAddress.getByName(SERVER_NAME), SettingsRMI.APPLICATION_PORT);

			return (Application_I)RmiTools.connectionRemoteObjectBloquant(rmiURL, delayMs, nbTentativeMax);
			}
		catch (UnknownHostException e)
			{
			System.err.println("[Application]: fail to reach host: " + e);
			e.printStackTrace();
			return null;
			}
		catch (RemoteException e)
			{
			System.err.println("[Application]: " + e);
			e.printStackTrace();
			}
		return null;
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	private Application_I remoteInstance;

	private JPanelReceive jPanelReceive;
	private JPanelSend jPanelSend;

	private JPanelRemoteVideo jPanelRemoteVideo;

	private PrivateKey privateKey;
	private PublicKey publicKey;

	private PublicKey remotePublicKey;

	/*------------------------------*\
	|*			  Static			*|
	\*------------------------------*/

	public static boolean IS_CONNECTED_TO_REMOTE_INSTANCE = false;
	private static Application INSTANCE = null;
	private static String PSEUDO = null;
	private static String SERVER_NAME = null;
	}
