﻿
package ch.hearc.gui.chat.text;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ch.hearc.gui.tools.Appearence;
import ch.hearc.gui.tools.ImageStore;
import ch.hearc.rmi.Application;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelSend extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelSend()
		{
		Application.getInstance().setjPanelSend(this);

		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	public void clearText()
		{
		textSend.setText(EMPTY);
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		buttonSend = new JButton();
		textSend = new JTextField(90);

		// Layout : Specification
			{
			FlowLayout flowlayout = new FlowLayout(FlowLayout.LEADING);
			setLayout(flowlayout);

			flowlayout.setHgap(20);
			flowlayout.setVgap(20);
			}

		// JComponent : add
		add(textSend);
		add(buttonSend);
		}

	private void control()
		{
		buttonSendListeners();

		addComponentListener(new ComponentAdapter()
			{

			@Override
			public void componentResized(ComponentEvent e)
				{
				textSend.setColumns(getWidth() / 15);
				}

			});

		textSend.addKeyListener(new KeyAdapter()
			{

			@Override
			public void keyPressed(KeyEvent e)
				{
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					{
					buttonSend.doClick();
					}
				}

			});
		}

	private void buttonSendListeners()
		{
		buttonSend.addActionListener(new ActionListener()
			{

			@Override
			public void actionPerformed(ActionEvent e)
				{
				Application.getInstance().sendMessage(textSend.getText());
				}
			});

		buttonSend.addMouseListener(new MouseAdapter()
			{

			@Override
			public void mouseExited(MouseEvent e)
				{
				buttonSend.setIcon(ImageStore.SEND);
				}

			@Override
			public void mouseEntered(MouseEvent e)
				{
				buttonSend.setIcon(ImageStore.SEND_ACTIVE);
				}
			});

		}

	private void appearance()
		{
		setBackground(Appearence.CYAN50);

		buttonSend.setIcon(ImageStore.SEND);
		buttonSend.setContentAreaFilled(false);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JButton buttonSend;
	private JTextField textSend;

	/*------------------------------*\
	|*			  Static			*|
	\*------------------------------*/

	private static String EMPTY = "";
	}
