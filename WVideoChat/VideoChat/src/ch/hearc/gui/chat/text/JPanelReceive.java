﻿
package ch.hearc.gui.chat.text;

import java.awt.FlowLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import ch.hearc.gui.tools.Appearence;
import ch.hearc.rmi.Application;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelReceive extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelReceive()
		{
		Application.getInstance().setjPanelReceive(this);

		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	public void addText(String message)
		{
		textReceive.append(message + "\n");
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		textReceive = new JTextArea(8, 100);
		textReceive.setEditable(false);
		textReceive.setLineWrap(true);

		scrollPane = new JScrollPane(textReceive);

		// Layout : Specification
			{
			FlowLayout flowlayout = new FlowLayout(FlowLayout.LEADING);
			setLayout(flowlayout);

			flowlayout.setHgap(20);
			flowlayout.setVgap(20);
			}

		// JComponent : add
		add(scrollPane);
		}

	private void control()
		{
		// Autoscroll at the bottom
		DefaultCaret caret = (DefaultCaret)textReceive.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		addComponentListener(new ComponentAdapter()
			{

			@Override
			public void componentResized(ComponentEvent e)
				{
				// Update message area along with the window size
				textReceive.setColumns(getWidth() / 13);
				}

			});
		}

	private void appearance()
		{
		setBackground(Appearence.CYAN50);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JScrollPane scrollPane;
	private JTextArea textReceive;
	}
