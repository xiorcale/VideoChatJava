﻿
package ch.hearc.gui.chat.video.videopanel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.AffineTransform;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import ch.hearc.rmi.Application;
import ch.hearc.video.image.ImageTools;
import ch.hearc.video.image.WebcamImage;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelRemoteVideo extends JPanelVideo
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelRemoteVideo(JPanelLocalVideo jPanelLocalVideo)
		{
		this.jPanelLocalVideo = jPanelLocalVideo;

		webcamImage = null;
		scaledImage = null;

		Application.getInstance().setJPanelRemoteVideo(this);

		geometry();
		control();
		appearance();
		}

	public void updateLocalPanel()
		{
		updateHorizontalGap();
		jPanelLocalVideo.setLocation(horizontalGap, getHeight() - 100);
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	@Override
	protected void paintComponent(Graphics g)
		{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;

		AffineTransform transform = g2d.getTransform();
		drawVideoImage(g2d);
		g2d.setTransform(transform);
		}

	public void setRemoteWebcamImage(WebcamImage webcamImage)
		{
		// Retreived the image and scaled it for our own screen.
		this.webcamImage = webcamImage;
		scaledImage = webcamImage.scaledImage(getHeight(), getWidth());
		repaint();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation

		// Layout : Specification
			{
			//BorderLayout borderlayout = new BorderLayout();
			//setLayout(borderlayout);
			setLayout(null);
			}

		jPanelLocalVideo.setSize(new Dimension(100, 100));
		add(jPanelLocalVideo);
		}

	private void control()
		{

		addPropertyChangeListener(new PropertyChangeListener()
			{

			@Override
			public void propertyChange(PropertyChangeEvent evt)
				{
				if (evt.getSource() instanceof Integer)
					{
					jPanelLocalVideo.setLocation(horizontalGap, getHeight() - 100);
					}
				}
			});

		addComponentListener(new ComponentAdapter()
			{

			@Override
			public void componentResized(ComponentEvent e)
				{
				// Update the location of the local video along with the window size
				updateDimensions();
				jPanelLocalVideo.setLocation(horizontalGap, getHeight() - 100);
				}

			});
		}

	private void appearance()
		{
		setOpaque(false);
		}

	private void drawVideoImage(Graphics2D g2d)
		{
		if (webcamImage != null)
			{
			ImageTools.processingImage(g2d, scaledImage, webcamImage, horizontalGap, panelWidth, panelHeight);
			}
		}
	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Inputs
	private JPanelLocalVideo jPanelLocalVideo;
	}
