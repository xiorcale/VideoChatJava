﻿
package ch.hearc.gui.chat.video.videopanel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.AffineTransform;

import ch.hearc.specifications.video.JPanelVideo_I;
import ch.hearc.video.controller.VideoControl;
import ch.hearc.video.image.ImageTools;
import ch.hearc.video.image.WebcamImage;

/**
 * JPanel which can display the user's video, and send it to a remote user.
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelLocalVideo extends JPanelVideo implements JPanelVideo_I
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelLocalVideo()
		{
		// Tools
			{
			webcamImage = null;
			panelWidth = 1; //  Avoid resizing an image to 0 at startup,
			panelHeight = 1; // which leads to an error!
			}

		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/**
	 * init the webcam and and give it to the VideoControl.
	 */
	public void initVideo()
		{
		webcamImage = new WebcamImage();
		VideoControl.getInstance().initVideoControl(this, webcamImage);
		}

	/**
	 * Disable the video
	 */
	public void disableVideo()
		{
		webcamImage.disableWebcam();
		webcamImage = null;
		}

	@Override
	public void changeMirrorState()
		{
		webcamImage.changeMirrorState();
		}

	@Override
	public void changeGrayscale()
		{
		webcamImage.changeGrayscale();
		}

	public boolean getGrayscale()
		{
		return webcamImage.isGrayscale();
		}

	@Override
	public void changeFPSDisplayed()
		{
		webcamImage.changeFPSDisplayed();
		}

	@Override
	public void changeImageSizeDisplayed()
		{
		webcamImage.changeImageSizeDisplayed();
		}

	@Override
	protected void paintComponent(Graphics g)
		{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;

		AffineTransform transform = g2d.getTransform();
		if (webcamImage != null)
			{
			drawVideoImage(g2d);
			}
		g2d.setTransform(transform);
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void control()
		{
		addComponentListener(new ComponentAdapter()
			{

			@Override
			public void componentResized(ComponentEvent e)
				{
				updateDimensions();
				}

			});
		}

	private void appearance()
		{
		setOpaque(false);

		setVisible(false);

		Dimension dimension = new Dimension(300, 100);
		setMinimumSize(dimension);
		setPreferredSize(dimension);
		}

	/**
	 * Send the image to the remote user. Then, make some image processing and display it.
	 * @param g2d Graphical context
	 */

	private void drawVideoImage(Graphics2D g2d)
		{
		// Resize the image in order to fit correctly the panel
		scaledImage = webcamImage.scaledImage(panelHeight, panelWidth);

		ImageTools.processingImage(g2d, scaledImage, webcamImage, 0, panelWidth, panelHeight);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	}
