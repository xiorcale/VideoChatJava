﻿
package ch.hearc.gui.chat.video;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import ch.hearc.gui.chat.video.videopanel.JPanelLocalVideo;
import ch.hearc.gui.chat.video.videopanel.JPanelRemoteVideo;
import ch.hearc.gui.tools.Appearence;
import ch.hearc.gui.tools.ImageStore;
import ch.hearc.video.controller.VideoControl;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelVideoAction extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelVideoAction(JPanelLocalVideo jPanelLocalVideo, JPanelRemoteVideo jPanelRemoteVideo)
		{
		this.jPanelLocalVideo = jPanelLocalVideo;
		this.jPanelRemoteVideo = jPanelRemoteVideo;

		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		buttonStartStop = new JButton();

		buttonMirrorState = new JButton();
		buttonMirrorState.setEnabled(false);

		buttonGrayscale = new JButton();
		buttonGrayscale.setEnabled(false);

		buttonLocalVideo = new JButton();
		buttonLocalVideo.setEnabled(false);

		// Layout : Specification
			{
			BorderLayout borderlayout = new BorderLayout();
			setLayout(borderlayout);
			}

		Box hBox = createBox();

		// JComponent : add
		add(hBox, BorderLayout.CENTER);
		}

	private Box createBox()
		{
		Box vBox = Box.createVerticalBox();

		vBox.add(Box.createVerticalGlue());
		vBox.add(buttonStartStop);
		vBox.add(Box.createVerticalGlue());
		vBox.add(buttonMirrorState);
		vBox.add(Box.createVerticalGlue());
		vBox.add(buttonGrayscale);
		vBox.add(Box.createVerticalGlue());
		vBox.add(buttonLocalVideo);
		vBox.add(Box.createVerticalGlue());

		Box hBox = Box.createHorizontalBox();

		hBox.add(Box.createHorizontalGlue());
		hBox.add(vBox);
		hBox.add(Box.createHorizontalGlue());

		return hBox;
		}

	private void control()
		{
		buttonStartStopListeners();
		buttonMirrorStateListeners();
		buttonGrayscaleListeners();
		buttonLocalVideoListeners();
		}

	private void buttonLocalVideoListeners()
		{
		buttonLocalVideo.addActionListener(new ActionListener()
			{

			@Override
			public void actionPerformed(ActionEvent e)
				{
				jPanelLocalVideo.setVisible(!jPanelLocalVideo.isVisible());
				}
			});

		buttonLocalVideo.addMouseListener(new MouseAdapter()
			{

			@Override
			public void mouseExited(MouseEvent e)
				{
				if (jPanelLocalVideo.isVisible())
					{
					buttonLocalVideo.setIcon(ImageStore.PORTRAIT_KILL);
					}
				else
					{
					buttonLocalVideo.setIcon(ImageStore.PORTRAIT);
					}
				}

			@Override
			public void mouseEntered(MouseEvent e)
				{
				if (jPanelLocalVideo.isVisible())
					{
					buttonLocalVideo.setIcon(ImageStore.PORTRAIT_KILL_ACTIVE);
					}
				else
					{
					buttonLocalVideo.setIcon(ImageStore.PORTRAIT_ACTIVE);
					}
				}

			});
		}

	private void buttonGrayscaleListeners()
		{
		buttonGrayscale.addActionListener(new ActionListener()
			{

			@Override
			public void actionPerformed(ActionEvent e)
				{
				jPanelLocalVideo.changeGrayscale();

				if (VideoControl.getInstance().isRunning())
					{
					buttonGrayscale.setIcon(ImageStore.GRAYSCALE);
					}
				else
					{
					buttonGrayscale.setIcon(ImageStore.GRAYSCALE_KILL);
					}
				}
			});

		buttonGrayscale.addMouseListener(new MouseAdapter()
			{

			@Override
			public void mouseExited(MouseEvent e)
				{
				if (buttonGrayscale.isEnabled())
					{
					if (!jPanelLocalVideo.getGrayscale())
						{
						buttonGrayscale.setIcon(ImageStore.GRAYSCALE);
						}
					else
						{
						buttonGrayscale.setIcon(ImageStore.GRAYSCALE_KILL);
						}
					}
				}

			@Override
			public void mouseEntered(MouseEvent e)
				{
				if (buttonGrayscale.isEnabled())
					{
					if (!jPanelLocalVideo.getGrayscale())
						{
						buttonGrayscale.setIcon(ImageStore.GRAYSCALE_ACTIVE);
						}
					else
						{
						buttonGrayscale.setIcon(ImageStore.GRAYSCALE_KILL_ACTIVE);
						}
					}
				}
			});
		}

	private void buttonStartStopListeners()
		{
		buttonStartStop.addActionListener(new ActionListener()
			{

			@Override
			public void actionPerformed(ActionEvent e)
				{
				if (VideoControl.getInstance().isRunning())
					{
					jPanelLocalVideo.disableVideo();
					VideoControl.getInstance().stopVideo();
					buttonStartStop.setIcon(ImageStore.WEBCAM);

					buttonMirrorState.setEnabled(false);
					buttonGrayscale.setEnabled(false);
					buttonLocalVideo.setEnabled(false);
					}
				else
					{
					new Thread(new Runnable()
						{

						@Override
						public void run()
							{
							jPanelLocalVideo.initVideo();
							VideoControl.getInstance().startVideo();

							buttonMirrorState.setEnabled(true);
							buttonGrayscale.setEnabled(true);
							buttonLocalVideo.setEnabled(true);

							buttonStartStop.setIcon(ImageStore.WEBCAM_KILL);

							jPanelRemoteVideo.updateLocalPanel();
							jPanelLocalVideo.updateHorizontalGap();
							}
						}).start();
					}
				}
			});

		buttonStartStop.addMouseListener(new MouseAdapter()
			{

			@Override
			public void mouseExited(MouseEvent e)
				{
				if (!VideoControl.getInstance().isRunning())
					{
					buttonStartStop.setIcon(ImageStore.WEBCAM);
					}
				else
					{
					buttonStartStop.setIcon(ImageStore.WEBCAM_KILL);
					}
				}

			@Override
			public void mouseEntered(MouseEvent e)
				{
				if (!VideoControl.getInstance().isRunning())
					{
					buttonStartStop.setIcon(ImageStore.WEBCAM_ACTIVE);
					}
				else
					{
					buttonStartStop.setIcon(ImageStore.WEBCAM_KILL_ACTIVE);
					}
				}

			});
		}

	private void buttonMirrorStateListeners()
		{
		buttonMirrorState.addActionListener(new ActionListener()
			{

			@Override
			public void actionPerformed(ActionEvent e)
				{
				jPanelLocalVideo.changeMirrorState();
				}
			});

		buttonMirrorState.addMouseListener(new MouseAdapter()
			{

			@Override
			public void mouseExited(MouseEvent e)
				{
				buttonMirrorState.setIcon(ImageStore.MIRROR);
				}

			@Override
			public void mouseEntered(MouseEvent e)
				{
				buttonMirrorState.setIcon(ImageStore.MIRROR_ACTIVE);
				}

			});
		}

	private void appearance()
		{
		setBackground(Appearence.CYAN700);

		buttonStartStop.setIcon(ImageStore.WEBCAM);
		buttonStartStop.setContentAreaFilled(false);

		buttonMirrorState.setIcon(ImageStore.MIRROR);
		buttonMirrorState.setContentAreaFilled(false);

		buttonGrayscale.setIcon(ImageStore.GRAYSCALE);
		buttonGrayscale.setContentAreaFilled(false);

		buttonLocalVideo.setIcon(ImageStore.PORTRAIT);
		buttonLocalVideo.setContentAreaFilled(false);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	//Inputs
	private JPanelLocalVideo jPanelLocalVideo;
	private JPanelRemoteVideo jPanelRemoteVideo;

	// Tools
	private JButton buttonStartStop;
	private JButton buttonMirrorState;
	private JButton buttonGrayscale;
	private JButton buttonLocalVideo;
	}
