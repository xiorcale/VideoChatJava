
package ch.hearc.gui.tools;

import java.awt.Color;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class Appearence
	{

	/*------------------------------*\
	|*			  Static			*|
	\*------------------------------*/

	//Colors
	public static final Color CYAN50 = Color.decode("#e0f7fa");
	public static final Color CYAN100 = Color.decode("#b2ebf2");
	public static final Color CYAN200 = Color.decode("#80deea");
	public static final Color CYAN300 = Color.decode("#4dd0e1");
	public static final Color CYAN400 = Color.decode("#26c6da");
	public static final Color CYAN500 = Color.decode("#00bcd4");
	public static final Color CYAN600 = Color.decode("#00acc1");
	public static final Color CYAN700 = Color.decode("#0097a7");
	public static final Color CYAN800 = Color.decode("#00838f");
	public static final Color CYAN900 = Color.decode("#006064");
	}
