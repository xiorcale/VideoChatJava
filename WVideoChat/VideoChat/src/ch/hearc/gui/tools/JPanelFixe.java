
package ch.hearc.gui.tools;

import java.awt.Dimension;

import javax.swing.JPanel;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class JPanelFixe extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelFixe(int size)
		{
		this.size = size;

		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// nothing
		}

	private void control()
		{
		// nothing
		}

	private void appearance()
		{
		dimension = new Dimension(size, size);
		setMinimumSize(dimension);
		setMaximumSize(dimension);
		setPreferredSize(dimension);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	//Inputs
	private int size;

	// Tools
	private Dimension dimension;
	}
