
package ch.hearc.gui.connection.structure;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ch.hearc.gui.chat.JFrameChat;
import ch.hearc.gui.connection.JFrameConnection;
import ch.hearc.gui.tools.Appearence;
import ch.hearc.gui.tools.ImageStore;
import ch.hearc.rmi.Application;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class JPanelConnection extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelConnection(JFrameConnection frameConnection)
		{
		this.frameConnection = frameConnection;

		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		labelRemoteIP = new JLabel("Remote IP adress : ");
		textRemoteIP = new JTextField("127.0.0.1");

		labelPseudo = new JLabel("Your pseudo : ");
		textPseudo = new JTextField(System.getProperty("user.name"));

		buttonConnect = new JButton("Connect");

		// Layout : Specification
			{
			FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
			setLayout(flowLayout);
			}

		Box layout = createBoxLayout();

		// JComponent : add
		add(layout, BorderLayout.CENTER);
		}

	private Box createBoxLayout()
		{
		Box vBox = Box.createVerticalBox();
		vBox.add(Box.createVerticalStrut(40));

		//First line
		Box hBox = Box.createHorizontalBox();
		hBox.add(labelRemoteIP);
		hBox.add(Box.createHorizontalStrut(20));
		hBox.add(textRemoteIP);
		vBox.add(hBox);
		vBox.add(Box.createVerticalStrut(20));

		//Second line
		hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalStrut(39));
		hBox.add(labelPseudo);
		hBox.add(Box.createHorizontalStrut(20));
		hBox.add(textPseudo);
		vBox.add(hBox);
		vBox.add(Box.createVerticalStrut(40));

		//Third line
		hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalStrut(265));
		hBox.add(buttonConnect);
		vBox.add(hBox);

		Dimension dimension = new Dimension(400, 250);
		vBox.setPreferredSize(dimension);
		vBox.setMinimumSize(dimension);
		vBox.setMaximumSize(dimension);

		return vBox;
		}

	private void control()
		{
		buttonConnect.addActionListener(new ActionListener()
			{

			@Override
			public void actionPerformed(ActionEvent e)
				{
				System.out.println("Launch of the application");

				if (checkInput())
					{
					new Thread(new Runnable()
						{

						@Override
						public void run()
							{
							connection();
							}

						}).start();
					}
				}

			private boolean checkInput()
				{
				if (IPV4_PATTERN.matcher(textRemoteIP.getText()).matches())
					{
					if (!textPseudo.getText().isEmpty())
						{
						return true;
						}
					else
						{
						JOptionPane.showMessageDialog(null, "Please enter a pseudo.", "Error", JOptionPane.ERROR_MESSAGE);
						return false;
						}
					}
				else
					{
					JOptionPane.showMessageDialog(null, "IP adresse is not valid.", "Error", JOptionPane.ERROR_MESSAGE);
					return false;
					}
				}

			private void connection()
				{
				buttonConnect.setEnabled(false);

				Application.init(textRemoteIP.getText(), textPseudo.getText());
				Application.getInstance().run();

				// Wait for the application to connect
				while(!Application.IS_CONNECTED_TO_REMOTE_INSTANCE)
					{
					System.out.println("Wait for the application to connect...");
					try
						{
						Thread.sleep(1000);
						}
					catch (InterruptedException e1)
						{
						e1.printStackTrace();
						}
					}

				new JFrameChat();

				frameConnection.setVisible(false);
				frameConnection.dispose();
				}
			});

		buttonConnect.addMouseListener(new MouseAdapter()
			{

			@Override
			public void mouseExited(MouseEvent e)
				{
				buttonConnect.setIcon(ImageStore.CONNECT);
				try
					{
					Font font = Font.createFont(Font.TRUETYPE_FONT, new File(getClass().getClassLoader().getResource("font/Roboto-Regular.ttf").getFile()));
					buttonConnect.setFont(font.deriveFont(18.0f));
					}
				catch (FontFormatException e1)
					{
					// TODO Auto-generated catch block
					e1.printStackTrace();
					}
				catch (IOException e1)
					{
					// TODO Auto-generated catch block
					e1.printStackTrace();
					}
				}

			@Override
			public void mouseEntered(MouseEvent e)
				{
				buttonConnect.setIcon(ImageStore.CLOSE);
				try
					{
					Font font = Font.createFont(Font.TRUETYPE_FONT, new File(getClass().getClassLoader().getResource("font/Roboto-Bold.ttf").getFile()));
					buttonConnect.setFont(font.deriveFont(18.0f));
					}
				catch (FontFormatException e1)
					{
					// TODO Auto-generated catch block
					e1.printStackTrace();
					}
				catch (IOException e1)
					{
					// TODO Auto-generated catch block
					e1.printStackTrace();
					}
				}
			});

		this.addKeyListener(new KeyAdapter()
			{

			@Override
			public void keyTyped(KeyEvent e)
				{
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					{
					buttonConnect.doClick();
					}
				}

			});
		}

	private void appearance()
		{
		setBackground(Appearence.CYAN50);
		Dimension dimension = new Dimension(150, 100);
		buttonConnect.setPreferredSize(dimension);
		buttonConnect.setIcon(ImageStore.CONNECT);
		buttonConnect.setContentAreaFilled(false);
		//Font
		try
			{
			Font font = Font.createFont(Font.TRUETYPE_FONT, new File(this.getClass().getClassLoader().getResource("font/Roboto-Regular.ttf").getFile()));
			labelPseudo.setFont(font.deriveFont(18.0f));
			labelRemoteIP.setFont(font.deriveFont(18.0f));
			buttonConnect.setFont(font.deriveFont(18.0f));
			}
		catch (FontFormatException | IOException e1)
			{
			e1.printStackTrace();
			}
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JLabel labelRemoteIP;
	private JLabel labelPseudo;

	private JTextField textRemoteIP;
	private JTextField textPseudo;

	private JButton buttonConnect;

	private JFrameConnection frameConnection;

	/*------------------------------*\
	|*			  Static			*|
	\*------------------------------*/

	private static final Pattern IPV4_PATTERN = Pattern.compile("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

	}
