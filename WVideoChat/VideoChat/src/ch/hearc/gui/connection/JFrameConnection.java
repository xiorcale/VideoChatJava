
package ch.hearc.gui.connection;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import ch.hearc.gui.connection.structure.JPanelConnection;
import ch.hearc.gui.connection.structure.JPanelConnectionFooter;
import ch.hearc.gui.connection.structure.JPanelConnectionHeader;
import ch.hearc.gui.tools.Appearence;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class JFrameConnection extends JFrame
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JFrameConnection()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		panelConnection = new JPanelConnection(this);
		panelConnectionHeader = new JPanelConnectionHeader();
		panelConnectionFooter = new JPanelConnectionFooter();

		// Layout : Specification
			{
			BorderLayout borderLayout = new BorderLayout();
			setLayout(borderLayout);
			}

		// JComponent : add
		add(panelConnectionHeader, BorderLayout.NORTH);
		add(panelConnection, BorderLayout.CENTER);
		add(panelConnectionFooter, BorderLayout.SOUTH);
		}

	private void control()
		{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		}

	private void appearance()
		{
		setBackground(Appearence.CYAN50);

		setMinimumSize(new Dimension(450, 700));
		setSize(450, 700);

		setLocationRelativeTo(null); // frame centrer

		setTitle("Java Chat 2.0 - Connection");

		setVisible(true); // last!
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JPanelConnectionHeader panelConnectionHeader;
	private JPanelConnection panelConnection;
	private JPanelConnectionFooter panelConnectionFooter;

	}
