
package ch.hearc.video.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;

import javax.swing.GrayFilter;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class ImageTools
	{

	/*------------------------------*\
	|*			  Static			*|
	\*------------------------------*/

	/**
	 * Snippet from https://stackoverflow.com/questions/13605248/java-converting-image-to-bufferedimage?answertab=active#tab-top
	 * @param Image
	 * @return BufferedImage
	 */
	public static BufferedImage toBufferedImage(Image image)
		{
		if (image instanceof BufferedImage) { return (BufferedImage)image; }

		BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = bufferedImage.createGraphics();
		g2d.drawImage(image, 0, 0, null);
		g2d.dispose();

		return bufferedImage;
		}

	/**
	 * Process an image and draw it thanks to the given graphical context.
	 * @param g2d Graphical context
	 * @param scaledImage The image which will be displayed
	 * @param webcamImage The processing that the webcam want us to do
	 * @param horizontalGap Used to center the image
	 * @param panelWidth int Width of the panel in which the image is displayed
	 * @param panelHeight int Height of the panel in which the image is displayed
	 */
	public static void processingImage(Graphics2D g2d, Image scaledImage, WebcamImage webcamImage, int horizontalGap, int panelWidth, int panelHeight)
		{
		if (webcamImage.isGrayscale())
			{
			scaledImage = grayScaleFilter(scaledImage);
			}

		if (webcamImage.isMirrored())
			{
			g2d.drawImage(scaledImage, -horizontalGap, 0, panelWidth - horizontalGap, panelHeight, panelWidth, 0, 0, panelHeight, null);
			}
		else
			{
			g2d.drawImage(scaledImage, horizontalGap, 0, panelWidth + horizontalGap, panelHeight, 0, 0, panelWidth, panelHeight, null);
			}

		if (webcamImage.isFPSDisplayed())
			{
			g2d.setColor(Color.RED);
			g2d.drawString((int)webcamImage.getWebcam().getFPS() + " FPS", panelWidth - 50, panelHeight - 10);
			}

		if (webcamImage.isImageSizeDisplayed())
			{
			StringBuilder string = new StringBuilder();
			string.append("Width: ");
			string.append(scaledImage.getWidth(null));
			string.append("     ");
			string.append("Height: ");
			string.append(scaledImage.getHeight(null));

			g2d.setColor(Color.RED);
			g2d.drawString(string.toString(), panelWidth - 200, panelHeight - 10);
			}
		}

	/**
	* Transform a colored image to a gray scale image.
	* @param image The colored image
	* @return Image The gray scale image
	*/
	private static Image grayScaleFilter(Image image)
		{
		ImageFilter filter = new GrayFilter(true, 20);
		ImageProducer imageProducer = new FilteredImageSource(image.getSource(), filter);
		return Toolkit.getDefaultToolkit().createImage(imageProducer);
		}

	}
