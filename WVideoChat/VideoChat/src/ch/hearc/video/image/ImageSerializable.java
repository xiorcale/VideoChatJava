
package ch.hearc.video.image;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.imageio.ImageIO;

public class ImageSerializable implements Serializable
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public ImageSerializable()
		{

		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------*\
	|*				Set				*|
	\*------------------------------*/

	public void setImage(Image image)
		{
		this.image = image;
		}

	/*------------------------------*\
	|*				Get				*|
	\*------------------------------*/

	public Image getImage()
		{
		return this.image;
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	/**
	* Customisation sérialisation
	*/
	private void writeObject(ObjectOutputStream oss)
		{
		try
			{
			image = ImageTools.toBufferedImage(image);
			ImageIO.write((RenderedImage)image, "jpeg", oss);
			}
		catch (IOException e)
			{
			e.printStackTrace();
			System.err.println("error while writing image");
			}
		}

	/**
	* Customisation désérialisation
	*/
	private void readObject(ObjectInputStream ois)
		{
		try
			{
			this.image = ImageIO.read(ois);
			}
		catch (IOException e)
			{
			e.printStackTrace();
			System.err.println("Error while reading image");
			}
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// transient -> l'attribut ne sera pas sérialisé
	// Rappel: BufferedImage n'est pas sérialisable
	private transient Image image;
	}
