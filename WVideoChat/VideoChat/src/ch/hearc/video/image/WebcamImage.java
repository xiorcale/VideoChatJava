
package ch.hearc.video.image;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import com.github.sarxos.webcam.Webcam;

import ch.hearc.gui.tools.Appearence;
import ch.hearc.specifications.video.FactoryVideo;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class WebcamImage implements Serializable
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public WebcamImage()
		{
		webcam = FactoryVideo.createWebcam();

		image = new ImageSerializable();

		// Ratio of the original image
		imageRatio = webcam.getImage().getWidth(null) / webcam.getImage().getHeight(null);

		isGrayscale = false;
		isImageSizeDisplayed = false;
		isFPSDisplayed = false;
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	public void disableWebcam()
		{
		webcam.close();
		}

	public void updateImge()
		{
		if (webcam.isOpen())
			{
			Image img = webcam.getImage();
			image.setImage(img);
			}
		else
			{
			// Create new image
			BufferedImage img = new BufferedImage(image.getImage().getWidth(null), image.getImage().getHeight(null), BufferedImage.TYPE_INT_ARGB);

			// Color the pixels with the same color than the background
			for(int i = 0; i < img.getHeight(); i++)
				{
				for(int j = 0; j < img.getWidth(); j++)
					{
					img.setRGB(j, i, Appearence.CYAN700.getRGB());
					}
				}

			image.setImage(img);
			}
		}

	public void changeMirrorState()
		{
		isMirrored = !isMirrored;
		}

	public void changeGrayscale()
		{
		isGrayscale = !isGrayscale;
		}

	public void changeFPSDisplayed()
		{
		isFPSDisplayed = !isFPSDisplayed;
		}

	public void changeImageSizeDisplayed()
		{
		isImageSizeDisplayed = !isImageSizeDisplayed;
		}

	public Image scaledImage(int panelHeight, int panelWidth)
		{
		return image.getImage().getScaledInstance(panelHeight * imageRatio, panelHeight, Image.SCALE_DEFAULT);
		}

	/*------------------------------*\
	|*				Get				*|
	\*------------------------------*/

	public Image getImage()
		{
		return image.getImage();
		}

	public int getImageRatio()
		{
		return imageRatio;
		}

	public boolean isMirrored()
		{
		return isMirrored;
		}

	public boolean isGrayscale()
		{
		return isGrayscale;
		}

	public boolean isImageSizeDisplayed()
		{
		return isImageSizeDisplayed;
		}

	public boolean isFPSDisplayed()
		{
		return isFPSDisplayed;
		}

	public Webcam getWebcam()
		{
		return webcam;
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private transient Webcam webcam;

	private ImageSerializable image;
	private int imageRatio;

	private boolean isMirrored;
	private boolean isGrayscale;
	private boolean isImageSizeDisplayed;
	private boolean isFPSDisplayed;
	}
