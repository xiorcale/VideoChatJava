
package ch.hearc.specifications.video;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public interface JPanelVideo_I
	{

	public void changeMirrorState();

	public void changeGrayscale();

	public void changeFPSDisplayed();

	public void changeImageSizeDisplayed();
	}
