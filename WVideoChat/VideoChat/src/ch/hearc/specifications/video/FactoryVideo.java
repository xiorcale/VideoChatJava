
package ch.hearc.specifications.video;

import java.awt.Dimension;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class FactoryVideo
	{

	/**
	 * Create a JPanel for displaying video
	 * @return JPanelVideo_I
	 */
	public static JPanelVideo_I createJPanelVideo()
		{
		return null;
		}

	/**
	 * Retreiving the default camera. Snippet from chat_video_001.pdf
	 * @return Webcam
	 */
	public static Webcam createWebcam()
		{
		Webcam webcam = Webcam.getDefault();

		Dimension resolution = new Dimension(1920, 1080);
		Dimension[] tabAlternateResolution = new Dimension[] { resolution, WebcamResolution.HD720.getSize(), WebcamResolution.VGA.getSize() };

		webcam.setCustomViewSizes(tabAlternateResolution);
		webcam.setViewSize(resolution);
		webcam.open();

		return webcam;
		}
	}
