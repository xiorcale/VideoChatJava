
package ch.hearc.specifications.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.PublicKey;

import ch.hearc.encryption.Message;
import ch.hearc.video.image.WebcamImage;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public abstract interface Application_I extends Remote
	{

	public abstract void savePublicKey(PublicKey publicKey) throws RemoteException;

	public abstract void readMessage(Message paramString) throws RemoteException;

	public abstract void readImage(WebcamImage webcamImage) throws RemoteException;
	}
