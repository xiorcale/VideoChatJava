
package ch.hearc.videochat;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import ch.hearc.gui.connection.JFrameConnection;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class UseVideoChat
	{
	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	public static void main(String[] args)
		{
		main();
		}

	public static void main()
		{
		appearence();
		new JFrameConnection();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private static void appearence()
		{
		//Set the Nimbus look and feel
		for(javax.swing.UIManager.LookAndFeelInfo info:javax.swing.UIManager.getInstalledLookAndFeels())
			{
			if ("Nimbus".equals(info.getName()))
				{
				try
					{
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					}
				catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e)
					{
					e.printStackTrace();
					}
				break;
				}
			}

		//Set font for the entire program
		try
			{
			Font font = Font.createFont(Font.TRUETYPE_FONT, new File(UseVideoChat.class.getClassLoader().getResource("font/Roboto-Regular.ttf").getFile()));
			setUIFont(font.deriveFont(14.0f));

			}
		catch (FontFormatException | IOException e1)
			{
			e1.printStackTrace();
			}
		}

	private static void setUIFont(Font font)
		{
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while(keys.hasMoreElements())
			{
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value != null && value instanceof javax.swing.plaf.FontUIResource)
				{
				UIManager.put(key, font);
				}
			}
		}

	}
